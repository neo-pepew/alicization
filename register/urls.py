from django.urls import path
from .views import user_Register, log_in, log_out, auth

urlpatterns = [
    path('register/', user_Register, name='register'),
    # path('reg/', register, name='reg'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='logout'),
    path('sso/login/', auth, name='sso'),
]
