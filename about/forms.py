from django import forms
from .models import Testimoni

class TestimoniForm(forms.ModelForm):
    komentar_attrs = {
		'class': 'form-control',
		'placeholder': 'Enter your testimony here...'
	}

    komentar = forms.CharField(label="", required=False, max_length=300, widget=forms.TextInput(attrs=komentar_attrs))

    class Meta:
        model = Testimoni
        fields = '__all__'
