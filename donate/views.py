from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError
from django.http import HttpResponse, HttpResponseRedirect
from .forms import DonateForm
from .models import Donatur
from program.models import Program
from program.views import program

# Create your views here.

response = {}


def user_Donation(request, id):
    if not request.user.is_authenticated:
        return redirect('login')

    response['title'] = "Donate - Sinar Perak"
    response['program_list'] = Program.objects.get(id=1)
    response['donate_form'] = DonateForm
    return render(request, 'donate.html', response)


def donate(request):
    form = DonateForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        name = 'Anonymous' if request.POST['name'] == '' else request.POST['name']
        email = request.POST['email']
        program = Program.objects.get(id=1)
        donation = request.POST['donation']
        donatur_list = Donatur(name=name, email=email,
                               program=program, donation=donation)
        donatur_list.save()
        return HttpResponseRedirect('/donate/')
    else:
        return HttpResponseRedirect('/donate/')
