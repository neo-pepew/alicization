from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .models import Testimoni
from .forms import TestimoniForm

def about(request):
    testi_form = TestimoniForm()
    testi_message = Testimoni.objects.all()

    if request.method == 'POST':
        testi_form = TestimoniForm(request.POST)
        if testi_form.is_valid():
            testi_form.save()
            return HttpResponseRedirect("/about/")
    return render(request, 'aboutsinarperak.html', {'testi_form': testi_form, 'testi_message': testi_message})
