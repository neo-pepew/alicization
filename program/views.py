from django.shortcuts import render
from .models import Program
# Create your views here.


def program(request):
    data = {}
    data['title'] = "Program - Sinar Perak"
    data['programs'] = Program.objects.filter(highlight=False)
    try:
        data['highlight'] = Program.objects.get(highlight=True)
    except:
        data['highlight'] = {}
    return render(request, 'program.html', data)
