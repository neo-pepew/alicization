from django import forms

class DonateForm(forms.Form):

	donation_attrs = {
	'class' : 'form-control',
	'placeholder' : 'Enter donation amount (in IDR)'
	}

	name_attrs = {
	'class' : 'form-control',
	'placeholder' : 'Left it blank for Anonymous donation'
	}

	email_attrs = {
	'class' : 'form-control',
	'placeholder' : 'Enter your valid email'
	}

	name = forms.CharField(label="Full Name", required=False, widget=forms.TextInput(attrs=name_attrs))
	email = forms.EmailField(label="E-mail", required=True, widget=forms.EmailInput(attrs=email_attrs))
	donation = forms.CharField(label="Donation Amount", required=True, widget=forms.NumberInput(attrs=donation_attrs))

