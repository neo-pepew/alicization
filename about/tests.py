from django.test import TestCase, Client
from django.urls import resolve, reverse

from .views import *
from .models import *
from .forms import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class FunctionalTestABoutPage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTestAboutPage, self).setUp()

class AboutPageTest(TestCase):

    def test_about_url_exists(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_abouthtml(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'aboutsinarperak.html')

    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_about_view_no_status_render_correctly(self):
        empty_form = TestimoniForm()
        response = Client().get('/about/')
        self.assertContains(response, 'Testimonies')
