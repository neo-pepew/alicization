from django.urls import path
from .views import news
# url for app
urlpatterns = [
    path('', news, name='news')
]
