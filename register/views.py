from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import RegisterForm
from .models import User
import json
from django.contrib.auth import logout
from django_cas_ng.utils import get_cas_client, get_service_url
from django.contrib.auth import login
from django.contrib.auth.models import User


# Create your views here.


def user_Register(request):
    return render(request, 'register.html', {'title': 'Register - Sinar Perak'})


# def register(request):
#     if(request.body):
#         data = json.loads(request.body)
#         try:
#             User.objects.get(email=data['email'])
#             return JsonResponse({"exist": True, "success": False})
#         except:
#             try:
#                 User(**data).save()
#                 return JsonResponse({"exist": False, "success": True})
#             except:
#                 return JsonResponse({"exist": False, "success": False})

#     return JsonResponse({"exist": False, "success": False})


def log_in(request):
    if request.user.is_authenticated:
        return redirect('news')

    return render(request, 'login.html')


def log_out(request):
    logout(request)
    return redirect('news')


def auth(request, next_page=None):
    service_url = get_service_url(request, next_page)
    client = get_cas_client(service_url=service_url, request=request)

    ticket = request.GET.get('ticket')

    if ticket:

        username, attributes, pgtiou = client.verify_ticket(ticket)

        if not username:
            return HttpResponse("Invalid Ticket")

        email = username + '@ui.ac.id'
        full_name = attributes['nama'].split()
        first_name = " ".join(full_name[:len(full_name)//2])
        last_name = " ".join(full_name[len(full_name)//2:])

        username = username.lower().strip()

        account = User.objects.filter(username=username).first()

        if account is None:
            account = User.objects.create(
                email=email,
                first_name=first_name,
                last_name=last_name,
                username=username
            )

        login(request, account, backend='django.contrib.auth.backends.ModelBackend')

        return redirect('program')

    return HttpResponse("Something went wrong")
