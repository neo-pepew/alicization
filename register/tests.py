from django.test import TestCase, Client
from django.urls import resolve
from .views import user_Register
from .forms import RegisterForm
from .models import User
import json
import request

# Create your tests here.


class RegisterUnitTest(TestCase):
    def setUp(self):
        return User.objects.create(name='sidiq usman',
                                   birthdate='1997-02-13',
                                   email='sidiqusman13@gmail.com',
                                   password='abc12345678')

    def test_register_page_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_page_contains_register(self):
        response = self.client.get('/register/')
        self.assertContains(response, '<h2>Registration Form</h2>')

    def test_root_url_resolves_to_register_page(self):
        found = resolve('/register/')
        self.assertEqual(found.func, user_Register)

    # def test_register_url_add_is_exist(self):
    #     res = self.client.get('/reg/')
    #     self.assertEqual(res.status_code, 200)
    #     self.assertJSONEqual(str(res.content, encoding='utf8'), {
    #                          'exist': False, 'success': False})

    def test_model_can_register_new_user(self):
        counting_all_user = User.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_name_str(self):
        obj = self.setUp()
        self.assertEqual('sidiq usman', obj.__str__())

    # def test_register_url_post_can_add_user(self):
    #     data = {'name': 'Sidiq', 'birthdate': '1997-02-13',
    #             'email': 'sidiq@gmail.com', 'password': '12345678'}
    #     res = self.client.post('/reg/', json.dumps(data),
    #                            content_type='application/json')
    #     self.assertTrue(str(res.content, encoding='utf8'))

    # def test_register_url_post_cannot_same_email(self):
    #     data = {'name': 'Sidiq', 'birthdate': '1997-02-13',
    #             'email': 'sidiqusman13@gmail.com', 'password': '12345678'}
    #     res = self.client.post('/reg/', json.dumps(data),
    #                            content_type='application/json')
    #     self.assertTrue(str(res.content, encoding='utf8'))

    # def test_register_url_post_can_handle_error(self):
    #     data = {'name': 'Sidiq', 'birthdate': '',
    #             'email': 'f@sil.com', 'password': '12345678'}
    #     res = self.client.post('/reg/', json.dumps(data),
    #                            content_type='application/json')
    #     self.assertJSONEqual(str(res.content, encoding='utf8'), {
    #                          'exist': False, 'success': False})

    def test_login_url_can_visit(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_can_logout(self):
        Client().get('/logout/')
        response = self.client.get('/')
        self.assertNotContains(response, 'Selamat datang')

    def test_sso_can_get_ticket(self):
        response = Client().get('/sso/login/?ticket=ST-5421-fFFu42MdeeMcmfdsxFWK-sso.ui.ac.id')
        self.assertEqual(response.status_code, 200)

    def test_sso_error(self):
        response = self.client.get('/sso/login/')
        self.assertContains(response, 'Something went wrong')
