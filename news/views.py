from django.shortcuts import render
from django.http import HttpResponseRedirect

highlight_title = 'Tsunami Tertinggi di Palu Capai 11,3 Meter'
highlight_short_text = 'Titik tertinggi tsunami tercatat 11,3 meter, terjadi di Desa Tondo, Palu Timur, Kota Palu.'
news1_title = 'Rangkaian bencana di Palu yang perlu Anda ketahui'
news1_text = 'Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih dari 2.000 jenazah telah ditemukan. Namun, jumlah pasti korban meninggal dunia amat mungkin tidak akan diketahui mengingat sejumlah daerah per......'
news2_title = 'ACDM Didesak Koordinasi Bantuan Gempa Donggala-Palu'
news2_text = 'Komite Manajemen Bencana ASEAN (ASEAN Committee on Disaster Management / ACDM) mendapatkan desakan dari Perdana Menteri Malaysia, Mahathir Mohamad, untuk mengkoordinasikan pemberian bantuan kepada Indonesia untuk menangani dampak ......'

def news(request):
    response = {'highlight_title': highlight_title, 'highlight_short_text': highlight_short_text,
                'news1_title': news1_title, 'news1_text': news1_text,
                'news2_title': news2_title, 'news2_text': news2_text}
    return render(request, 'newspage.html', response)
