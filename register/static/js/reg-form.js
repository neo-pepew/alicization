"use strict";
var _createClass = function() {
  function defineProperties(target,props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor=props[i];
      descriptor.enumerable = descriptor.enumerable||false;
      descriptor.configurable = true;
      if ("value"in descriptor) {
        descriptor.writable = true;
      }
      Object.defineProperty(target,descriptor.key,descriptor)
    }
  }
  return function(Constructor, protoProps, staticProps) {
    if (protoProps) {
      defineProperties(Constructor.prototype,protoProps);
    }
    if(staticProps) {
      defineProperties(Constructor,staticProps);
    }
    return Constructor}
  }();
  function _defineProperty(obj,key,value) {
    if (key in obj) {
      Object.defineProperty(obj,key, {
        value:value,
        enumerable:true,
        configurable:true,
        writable:true
      })
    }
    else {obj[key]=value} return obj
  }
  function _classCallCheck(instance,Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function")
    }
  }
  function _possibleConstructorReturn(self,call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called")
    }
    return call&&(typeof call==="object"||typeof call==="function")?call:self
  }
  function _inherits(subClass,superClass) {
    if (typeof superClass!=="function"&&superClass!==null) {
      throw new TypeError("Super expression must either be null or a function, not "+typeof superClass)
    }
    subClass.prototype = Object.create(superClass&&superClass.prototype,{constructor: {value:subClass,enumerable:false,writable:true,configurable:true
    }
  });
  if (superClass) {
    Object.setPrototypeOf ? Object.setPrototypeOf(subClass,superClass) : subClass.__proto__ = superClass
  }
  var FormRegister = function(_React$Component) {
    _inherits(FormRegister,_React$Component);
    function FormRegister() {
      var _ref;
      var _temp,
      _this,
      _ret;
      _classCallCheck(this,FormRegister);
      for (var _len = arguments.length, args = Array(_len), _key=0; _key < _len; _key++) {
        args[_key] = arguments[_key]
      }
      return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = FormRegister.__proto__||Object.getPrototypeOf(FormRegister)).call.apply(_ref,[this].concat(args))),_this),
      _this.state = { name:"", email:"", birthdate:"", password:"", isLoading:false, isSuccess:false, isExist:false },
      _this.handleChange = function(e){
        _this.setState(_defineProperty({},e.target.name,e.target.value))
      },
      _this.handleSubmit = function(e) {
        var_this$state = _this.state,
        name = _this$state.name,
        email = _this$state.email,
        birthdate = _this$state.birthdate,
        password = _this$state.password;
        e.preventDefault();
        if (name && email && birthdate && password) {
          _this.setState({isLoading:true});
          superagent.post("/reg/").set("X-CSRFToken",_this.props.token).set("content-type","application/json").send({name:name, email:email, birthdate:birthdate, password:password}).then(function(res) {
            console.log(res.body);
            _this.setState({isLoading:false, isSuccess:res.body.success, isExist:res.body.exist})
          })
        }
      }
      ,_temp)
      ,_possibleConstructorReturn(_this,_ret)}
      _createClass(FormRegister,[{key:"render", value:function render() {
        var _this2 = this;
        var _state = this.state,
        isLoading = _state.isLoading,
        isSuccess = _state.isSuccess,
        isExist = _state.isExist;
        if (isSuccess) {
          setTimeout(function(){window.location.replace("/")},1e3)
        }
        return React.createElement(React.Fragment,null,React.createElement("form", { onSubmit:function onSubmit(e) {
          return _this2.handleSubmit(e)}
        },
        isSuccess && React.createElement("div", { "class":"alert alert-success"}, "You Are successfully registered"),
        React.createElement("div", { "class":"form-group"}, React.createElement("label", { "for":"name"}, "Full Name"),
        React.createElement("input", { type:"text", "class":"form-control", id:"name", name:"name", onChange:function onChange(e) {
          return _this2.handleChange(e)
        },
        required:true })),
        React.createElement("div",{"class":"form-group"},React.createElement("label",{"for":"birthdate"},"Birth Date"),React.createElement("input",{type:"date","class":"form-control",id:"birthdate",name:"birthdate",onChange:function onChange(e){return _this2.handleChange(e)},required:true})),React.createElement("div",{"class":"form-group"},React.createElement("label",{"for":"email"},"E-mail"),React.createElement("input",{type:"email","class":"form-control",id:"email",name:"email",onChange:function onChange(e){return _this2.handleChange(e)},required:true}),isExist&&React.createElement("small",{style:{color:"red"}},"Email already taken")),React.createElement("div",{"class":"form-group"},React.createElement("label",{"for":"password"},"Password"),React.createElement("input",{type:"password","class":"form-control",id:"password",name:"password",onChange:function onChange(e){return _this2.handleChange(e)},required:true})),React.createElement("button",{type:"submit","class":"btn btn-primary",disabled:isLoading},isLoading?React.createElement("i",{"class":"fa fa-spinner fa-spin"}):"Register")))}}]);return FormRegister}(React.Component);
