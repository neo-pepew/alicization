from django.db import models

class Testimoni(models.Model):

    komentar = models.CharField(max_length=300)

    def __str__(self):
        return self.komentar