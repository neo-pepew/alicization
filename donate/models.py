from django.db import models

# Create your models here.
class Donatur(models.Model):
	name = models.CharField(max_length=32, default="Anonymous", blank=False)
	email = models.EmailField(null=True)
	program = models.CharField(max_length=64, default="Anonymous", blank=False)
	donation = models.CharField(max_length=32)

	def __str__(self):
		return self.name