from django.urls import path
from .views import about
# url for app
urlpatterns = [
    path('about/', about, name='about')
]
