from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import news


class NewsTests(TestCase):

    def test_news_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_newspagehtml(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'newspage.html')

    def test_news_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, news)

    def test_news_has_correct_content(self):
        response = Client().get('/')
        self.assertContains(response, 'HIGHLIGHT NEWS')
        self.assertContains(
            response, 'Tsunami Tertinggi di Palu Capai 11,3 Meter')
        self.assertContains(
            response, 'Titik tertinggi tsunami tercatat 11,3 meter, terjadi di Desa Tondo, Palu Timur, Kota Palu.')
        self.assertContains(
            response, 'Rangkaian bencana di Palu yang perlu Anda ketahui')
        self.assertContains(response, 'Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih dari 2.000 jenazah telah ditemukan. Namun, jumlah pasti korban meninggal dunia amat mungkin tidak akan diketahui mengingat sejumlah daerah per......')
        self.assertContains(response, 'Read more . . . . .')
        self.assertContains(
            response, 'ACDM Didesak Koordinasi Bantuan Gempa Donggala-Palu')
        self.assertContains(response, 'Komite Manajemen Bencana ASEAN (ASEAN Committee on Disaster Management / ACDM) mendapatkan desakan dari Perdana Menteri Malaysia, Mahathir Mohamad, untuk mengkoordinasikan pemberian bantuan kepada Indonesia untuk menangani dampak ......')
        self.assertContains(response, 'Read more . . . . .')
