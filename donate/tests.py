from django.test import TestCase, Client
from django.urls import resolve
from .views import user_Donation
from .forms import DonateForm
from .models import Donatur
# import json
# import request

# Create your tests here.


class DonateUnitTest(TestCase):
    def setUp(self):
        return Donatur.objects.create(name='sidiq usman',
                                      email='sidiqusman13@gmail.com',
                                      donation='100000')

    def test_donate_page_is_exist(self):
        response = Client().get('/donate/1/')
        self.assertEqual(response.status_code, 302)

    # def test_donate_page_contains_donate(self):
    #     response = self.client.get('/donate/1/')
    #     self.assertContains(response, '<h2>Donation Form</h2>')

    # def test_root_url_resolves_to_register_page(self):
    #     found = resolve('/donate/')
    #     self.assertEqual(found.func, user_Donation)

    # def test_donate_url_add_is_exist(self):
    #     res = self.client.get('/donation/')
    #     self.assertEqual(res.status_code, 200)

    def test_model_can_donate_new_user(self):
        counting_all_user = Donatur.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_name_str(self):
        obj = self.setUp()
        self.assertEqual('sidiq usman', obj.__str__())
