from django.urls import path
from .views import user_Donation, donate


urlpatterns = [
    path('donate/<int:id>/', user_Donation, name='donate'),
    path('donation/', donate, name='donation'),
]
