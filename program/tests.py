from django.test import TestCase, Client
from .models import Program

# Create your tests here.


class ProgramUnitTest(TestCase):
    def setUp(self):
        return Program.objects.create(
            name='sinar perak', image='https://img.com/hehe', highlight=False)

    def test_program_url_is_exist(self):
        response = Client().get('/program/')
        self.assertEqual(response.status_code, 200)

    def test_program_page_contains_title(self):
        response = self.client.get('/program/')
        self.assertContains(response, '<h1>Available Donation Program</h1>')

    def test_models_add_and_get_right_data(self):
        obj = self.setUp()
        self.assertEqual('sinar perak', obj.name)
        self.assertEqual('https://img.com/hehe', obj.image)
        self.assertEqual(False, obj.highlight)

    def test_models_name_str(self):
        obj = self.setUp()
        self.assertEqual('sinar perak', obj.__str__())
