from django import forms

class RegisterForm(forms.Form):

	name_attrs = {
		'class': 'form-control',
		'placeholder': 'Enter your name...'
	}

	date_attrs = {
		'class': 'form-control',
		'placeholder': 'yyyy-mm-dd'
	}

	email_attrs = {
		'class': 'form-control',
		'placeholder': 'Enter your e-mail'
	}

	password_attrs = {
		'class': 'form-control',
		'placeholder': 'Enter valid password (min 8 characters)'
	}


	name = forms.CharField(label="Full Name", required=True, max_length=32, widget=forms.TextInput(attrs=name_attrs))
	birthdate = forms.DateField(label="Birth Date", required=True, widget=forms.DateInput(attrs=date_attrs))
	email = forms.EmailField(label="E-mail", required=True, widget=forms.EmailInput(attrs=email_attrs))
	password = forms.CharField(label="password", required=True, max_length=32, widget=forms.PasswordInput(attrs=password_attrs))